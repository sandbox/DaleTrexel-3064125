<?php

/**
 * @environment
 * Plugin definition for the Environment key provider.
 */

$plugin = array(
    'label' => t('Environment'),
    'description' => t('The Environment key provider allows a key to be stored as an environment variable, typically saved in settings.php or include file.'),
    'storage method' => 'environment',
    'key value' => array(
        'accepted' => FALSE,
        'required' => FALSE,
    ),
    'default configuration' => 'key_provider_environment_default_configuration',
    'build configuration form' => 'key_provider_environment_build_configuration_form',
    'validate configuration form' => 'key_provider_environment_validate_configuration_form',
    'get key value' => 'key_provider_environment_get_key_value',
);

/**
 * The default plugin configuration.
 *
 * @return array
 *   The default plugin configuration.
 */
function key_provider_environment_default_configuration() {
    return array(
        'environment_variable' => '',
        'base64_encoded' => FALSE,
    );
}

/**
 * Build the plugin configuration form.
 *
 * @return array
 *   The plugin configuration form.
 */
function key_provider_environment_build_configuration_form($form, &$form_state) {
    $config = $form_state['storage']['key_config'];
    $plugin_config = $form_state['storage']['key_config']['key_provider_settings'] + key_provider_environment_default_configuration();

    $form['environment_variable'] = array(
        '#type' => 'textfield',
        '#title' => t('Environment Variable Name'),
        '#description' => t('The environment variable name. Must be prefixed "key_" where stored, but do NOT include the prefix here.', array()),
        '#default_value' => $plugin_config['environment_variable'],
        '#required' => TRUE,
        '#field_prefix' => 'key_',
    );

    // If this is for an encryption key.
    $key_type = key_get_plugin('key_type', $config['key_type']);
    if ($key_type['group'] == 'encryption') {
        // Add an option to indicate that the value is Base64-encoded.
        $form['base64_encoded'] = array(
            '#type' => 'checkbox',
            '#title' => t('Base64-encoded'),
            // @TODO: confirm this is correct (it seems it only means DECRYPT when used)
            '#description' => t('Checking this will store the key with Base64 encoding.'),
            '#default_value' => $plugin_config['base64_encoded'],
        );
    }

    return $form;
}

/**
 * Validate callback for the configuration form.
 */
function key_provider_environment_validate_configuration_form($form, &$form_state) {
    $key_provider_settings = $form_state['values'];
    $environment_variable = variable_get('key_' . $key_provider_settings['environment_variable']);

    // Does the value exist and is it set?
    if (!isset($environment_variable)) {
        form_set_error('environment_variable', t('There is no environment variable with that machine name.'));
        return;
    }

}

/**
 * Callback function to return a key from an environment variable.
 */
function key_provider_environment_get_key_value($config) {

    // If no key configured with matching name, return nothing.
    if (empty($config['key_provider_settings']['environment_variable'])) {
        return NULL;
    }

    // Security prefix: all key variables are required to be prefixed with "key_"
    $environment_variable = variable_get('key_' . $config['key_provider_settings']['environment_variable']);

    // If no environment variable by that name exists, return nothing.
    if (!isset($environment_variable)) {
        return NULL;
    }

    // Base64-decode if necessary.
    if (isset($config['key_provider_settings']['base64_encoded']) && $config['key_provider_settings']['base64_encoded']) {
        $environment_variable = base64_decode($environment_variable);
    }

    return $environment_variable;
}
